#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "err.h"
#define bool int
#define true 1
#define false 0
#define t_length int
#define String int*
#define Dictionary int**

void init() //keep
{
    srand(time(NULL));
}

int randnum(int range)  //keep
{
    int k;
    if((k = rand()) == 1)
    {return 0;}
    return k % (range + 1);
    
}

String txtfromfile(char* filename) //keep
{
    t_length length = 0;

    FILE *file;
    file = fopen(filename,"r");
    if(file == NULL)
    {
        printf("Error opening file %s ",filename);
    }
    else
    {
        while(getc(file) != EOF)
        {
            length++;
        }
        rewind(file);
        //String text = malloc(length*sizeof(char));
        String text = malloc(length*sizeof(int));
        text[0] = length;   //this could be dangerous... maybe do length - 1
        //for(int i = 1; i < length; i++)
        for(int i = 1; i < length; i++)
        {
            text[i] = getc(file);
        }
        fclose(file);
        return text;

    }
}

void strngprint(String string) //keep
{
    if(string[0] == NO_LENDEF)
    {

        for(int i = 1; string[i] != 0; i++)
        {
            printf("%c",string[i]);
        }
    }else{
    for(int i = 1; i < string[0]; i++)
    {
        printf("%c",string[i]);
    }
    }
}

String upper(String string) //keep
{
    t_length LENGTH = string[0];
    String newstring = malloc(LENGTH*sizeof(int)); newstring[0]=string[0];
    
    if(LENGTH !=  NO_LENDEF){
    for(int i = 1; i < LENGTH; i++)
    {
        if(string[i] >= 97 && string[i] < 123)
        {
            newstring[i] = string[i] - 32;
        }
        else
        {
            newstring[i] = string[i];
        }
    }return newstring;}else{
    for(int i = 1;string[i] ; i++)
    {
        if(string[i] >= 97 && string[i] < 123)
        {
            newstring[i] = string[i] - 32;
        }
        else
        {
            newstring[i] = string[i];
        }
    }return newstring;}
}

int* countletters(String string) //keep
{
    t_length LENGTH = string[0];
    int* lettercount = calloc(52,sizeof(int));
    string = upper(string);
    
    for(int i = 0; i < 26; i++)
    {
        lettercount[i * 2 + 1] = i;
    }

    for(int i = 1; i < LENGTH; i++)
    {
        lettercount[(string[i] - 65)*2] += 1;
    }
    return lettercount;
}

String gw(String text, int pick)    //keep
{
    /*  This will be implemented by counting spaces between words. Only literal
     *  characters are returned, no dots, seperators etc. Because the length of
     *  the word cannot be known in advance, a "end-determninator" will be used
     *  The first block of the string will say NO_LENDEF, which means no len is
     *  defined and the WORDEND terminator will be used. Note that this functio
     *  n is used to get a word out of plain text, not an array. Max. wrdlen is
     *  25, which means words that consist of more than 25 characters won't  be
     *  used. Don't be afraid, a segmentation fault will not occur
     */ 
    int i,o,c,k;
    text = upper(text);
    
    int words = 0, cursor = 0;
    
    t_length LENGTH = text[0];
    String word = calloc(25,sizeof(int));
    for(i = 1; i < LENGTH; i++)
    {
        if(text[i] == ' '){words++;}
        if(words == pick)
        {
            word[0] = NO_LENDEF;
            
            while(text[i] < 65 || text[i] > 90)
            {i++;}
            for(o = 1; o < 25; o++) 
            {
                if(text[i + o - 1] >= 65 && text[i + o - 1] <= 90)
                {
                    word[o] = text[i + o - 1]; 
                }else{return word;}   
            }
        }
    }
}


int ffp(String text, String pattern)
{
    int offset,pointadd,score = 0;
    for(offset = 0; offset <= text[0] - pattern[0]; offset++)
    {
        for(pointadd = 1; pointadd < pattern[0]; pointadd++)
        {
            if(text[offset + pointadd] == pattern[pointadd])
            {
                score++;
            } //maybe -1
        }
        if(score == (pattern[0] - 1))
        {
            return offset;
        }
        score = 0;
    }
    return NOTHING_FOUND;
}


String mkstr(char* rawstring)
{
    int i,o;
    for(i = 0; rawstring[i] != 0; i++){}
    String string = malloc(i*sizeof(int));

    for(o = 1; o < i + 1; o++)
    {
        string[o] = rawstring[o - 1]; 
    }
    string[0] = i + 1;
    return string;
}

String getft(String string, int from, int to)
{
    int i;
    int a = 0;
    if(to > string[0])
    {
        string[0] = OUT_OF_BOUNDS;
        return string;
    }
    String cropped = malloc(to - from + 2);
    for(i = from; i < to + 1; i++){cropped[++a] = string[i];}
    cropped[0] = a + 1;
    printf("LENGTH SEEMS TO BE: %d \n",a );
    return cropped;
}

void nl(){printf("\n");}
int main()
{
/*    char* query = malloc(25*sizeof(char));
    printf("QUERY: ");
    scanf("%s",query);
    String pattern = mkstr(query);
    printf("Lenght of pattern: %d \n", pattern[0]);
    String text = txtfromfile("text.txt");
    printf("Found at %d! \n ",ffp(text,pattern));
    free(query);
    free(text);
    return 0;*/
    String text = txtfromfile("test.txt");
    String debug = getft(text,2,4);
    strngprint(text);
    nl();
    strngprint(debug);
    printf("\nKike \n");
    return 0;
}


